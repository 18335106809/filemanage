package com.oraclechain.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/11
 */
@SpringBootApplication
public class FileManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileManageApplication.class);
    }
}
