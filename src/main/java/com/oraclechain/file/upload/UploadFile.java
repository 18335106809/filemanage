package com.oraclechain.file.upload;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.file.util.QRCodeUtils;
import com.oraclechain.file.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Map;
import java.util.UUID;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/11
 */
@RestController
@RequestMapping("/file")
public class UploadFile {

    static Logger logger = LoggerFactory.getLogger(UploadFile.class);

    @Value("${file.server.host}")
    private String fileHost;

    @Value("${file.server.store.path}")
    private String storePath;

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadForChat(@RequestParam(required = false) MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            return JSONObject.toJSONString(ResultUtil.error("图片不存在"));
        }
        //获取文件名称
        String fileName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString();
        String filePath = storePath + uuid;
        logger.info("file upload for chat, fileName: " + fileName);
//        String path = filePath + "\\" + fileName;
        String path = filePath + "/" + fileName;
        File dest = new File(path);
        try {
            //2.上传文件保存路径
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }

            file.transferTo(dest);
            logger.info("file upload "+ fileName +" success");
            String fileServerPath = "http://" + fileHost + "/files/" + uuid + "/" + fileName;
            return JSONObject.toJSONString(ResultUtil.success(fileServerPath));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("文件上传失败"));
        }
    }

    @RequestMapping(value = "QRcode", method = RequestMethod.POST)
    @ResponseBody
    public Object makeQRcode(@RequestBody Map<String, Object> map) {
        String invitationCode = map.get("invitationCode").toString();
        try {
            if (!StringUtils.isEmpty(invitationCode)) {
                String uuid = UUID.randomUUID().toString();
                String filePath = storePath + uuid;
                String fileName = QRCodeUtils.encode(invitationCode, null, filePath, true);
                String fileServerPath = "http://" + fileHost + "/files/" + uuid + "/" + fileName;
                return JSONObject.toJSONString(ResultUtil.success(fileServerPath));
            } else {
                return JSONObject.toJSONString(ResultUtil.error("the invitation code is null"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("make QR code failed"));
        }
    }

}
